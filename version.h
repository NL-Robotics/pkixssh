/* $OpenBSD: version.h,v 1.95 2022/09/26 22:18:40 djm Exp $ */

#define SSH_VERSION	"OpenSSH_9.1"

#define SSH_RELEASE	PACKAGE_STRING ", " SSH_VERSION "p1"
